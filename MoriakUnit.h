//---------------------------------------------------------------------------

#ifndef MoriakUnitH
#define MoriakUnitH
#include "trayicon.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ScktComp.hpp>
#include <StdCtrls.hpp>
#include <ValEdit.hpp>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// definicie sekund za casove useky
#define SEC_PER_MIN           60  // za minutu
#define SEC_PER_15MIN        900  // za 15 minut
#define SEC_PER_HOUR        3600  // za hodinu
#define SEC_PER_DAY        86400  // za den 24*60*60
#define SEC_PER_WEEK      604800  // za tyzden 7*24*60*60
#define SEC_PER_MONTH    2678400  // za mesiac 31*24*60*60
#define SEC_PER_YEAR    31622400  // za rok 366*24*60*60
#define DATEOFFSET1970 946684800  // Pocet sekund mezi 1.1.1970 a 1.1.2000

#define MAX_LOG_LINES      5000
#define MAX_TCP_PACKET_LEN  512
#define MAX_STRING_LEN      128
#define FILE_COMMAND  "command.mrk"
#define FILE_OUTPUT   "output.mrk"
#define FILE_ERROR    "error.mrk"
#define CSV_SEPARATOR   ";"

typedef enum {
  CMD_UNKNOWN,
  CMD_SET,
  CMD_GET,
  CMD_INC,
  CMD_STOP,
  CMD_START,
  CMD_SET_PERIOD,
  CMD_SET_COUNT,
  CMD_STOP_COUNT,
  CMD_MAKRO,
  CMD_WAIT,
  CMD_EXIT,
} enumCommand;

typedef enum {
  PRD_UNKNOWN,
  PRD_BACK_FLUSH,
  PRD_SCRAP,
  PRD_BREAK_DOWN,
} enumPeriod;

class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TTrayIcon *TrayIcon;
    TTimer *TimerTCP;
    TServerSocket *ServerSocket;
    TImageList *IconsList;
    TLabel *LabelCopyright;
    TPanel *PanelVariable;
    TSplitter *FormSplitter;
    TMemo *Log;
    TValueListEditor *ValueListEditor;
    TCheckBox *CheckBoxMinimalize;
    TLabel *LabelPort;
    TEdit *EditIPPort;
    TPopupMenu *PopupMenuTray;
    TMenuItem *Open;
    TMenuItem *Minimalize;
    TMenuItem *Exit;
    TPopupMenu *PopupMenuLog;
    TMenuItem *MenuItemSelectAll;
    TMenuItem *MenuItemDelete;
    TMenuItem *MenuItemCopy;
    TTimer *TimerBackFlush;
    TTimer *TimerScrap;
    TTimer *TimerCommands;
    TGroupBox *GroupBoxCounters;
    TLabel *LabelBackFlush;
    TLabel *LabelScrap;
    TLabel *LabelBreakDown;
    TLabel *LabelJednotky;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *EditBackFlush;
    TEdit *EditScrap;
    TEdit *EditBreakDown;
    TCheckBox *CheckBoxCounter;
    TEdit *EditScrapCounter;
    TEdit *EditBackFlushCounter;
    TGroupBox *GroupBoxMakro;
    TButton *ButtonMakro1;
    TButton *ButtonMakro2;
    TButton *ButtonMakro3;
    TButton *ButtonMakro4;
    TButton *ButtonMakro5;
    TButton *ButtonMakro6;
    TButton *ButtonMakro7;
    TButton *ButtonMakro8;
    TTimer *TimerMakro;
    TButton *ButtonMakro9;
    TTimer *TimerBreakDown;
    TButton *ButtonFreeze;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall LabelCopyrightClick(TObject *Sender);
    void __fastcall OpenClick(TObject *Sender);
    void __fastcall MinimalizeClick(TObject *Sender);
    void __fastcall ExitClick(TObject *Sender);
    void __fastcall MenuItemSelectAllClick(TObject *Sender);
    void __fastcall MenuItemDeleteClick(TObject *Sender);
    void __fastcall MenuItemCopyClick(TObject *Sender);
    void __fastcall TrayIconBlink();
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall TimerTCPTimer(TObject *Sender);
    void __fastcall EditIPPortChange(TObject *Sender);
    void __fastcall ServerSocketAccept(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketClientConnect(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketClientDisconnect(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketClientError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode);
    void __fastcall ServerSocketClientRead(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketClientWrite(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketGetSocket(TObject *Sender, int Socket, TServerClientWinSocket *&ClientSocket);
    void __fastcall ServerSocketGetThread(TObject *Sender, TServerClientWinSocket *ClientSocket, TServerClientThread *&SocketThread);
    void __fastcall ServerSocketListen(TObject *Sender, TCustomWinSocket *Socket);
    void __fastcall ServerSocketThreadEnd(TObject *Sender, TServerClientThread *Thread);
    void __fastcall ServerSocketThreadStart(TObject *Sender, TServerClientThread *Thread);
    void __fastcall LogChange(TObject *Sender);
    void __fastcall CheckBoxMinimalizeClick(TObject *Sender);
    void __fastcall EditBackFlushChange(TObject *Sender);
    void __fastcall TimerBackFlushTimer(TObject *Sender);
    void __fastcall EditScrapChange(TObject *Sender);
    void __fastcall TimerScrapTimer(TObject *Sender);
    void __fastcall TimerCommandsTimer(TObject *Sender);
    void __fastcall ButtonMakro1Click(TObject *Sender);
    void __fastcall ButtonMakro2Click(TObject *Sender);
    void __fastcall ButtonMakro3Click(TObject *Sender);
    void __fastcall ButtonMakro4Click(TObject *Sender);
    void __fastcall ButtonMakro5Click(TObject *Sender);
    void __fastcall ButtonMakro6Click(TObject *Sender);
    void __fastcall ButtonMakro7Click(TObject *Sender);
    void __fastcall ButtonMakro8Click(TObject *Sender);
    void __fastcall CheckBoxCounterClick(TObject *Sender);
    void __fastcall TimerMakroTimer(TObject *Sender);
    void __fastcall ButtonMakro9Click(TObject *Sender);
    void __fastcall EditBreakDownChange(TObject *Sender);
    void __fastcall TimerBreakDownTimer(TObject *Sender);
    void __fastcall ButtonFreezeClick(TObject *Sender);
private:	// User declarations
    AnsiString gpacket;
    TStrings *commands;
    AnsiString MyGetApplicationVersion(void);
    void readParamFile(AnsiString file_name);
    void writeParamFile(AnsiString file_name);
    void __fastcall UpdateModbusDateTime();
    AnsiString __fastcall ModbusFindInd(unsigned long ind);
    AnsiString __fastcall LoadMakroName(AnsiString FileName, AnsiString default_name);
    AnsiString __fastcall GetCSVValue(AnsiString *line);
    enumCommand __fastcall GetEnumCommand(AnsiString line);
    enumPeriod __fastcall GetEnumPeriod(AnsiString line);
    void __fastcall LoadToList(char* FileName);
    void __fastcall DoMakro();
    void __fastcall DoCommand(AnsiString line);
public:		// User declarations
    __fastcall TForm1(TComponent* Owner);
    AnsiString __fastcall StreamToHex(unsigned char packet[], unsigned short len);
    unsigned short __fastcall MakeAnswer(unsigned char packet[], unsigned short len);
    unsigned short __fastcall ModbusReadCoil(unsigned long Address, unsigned short Quantity, unsigned char packet[]);
    unsigned short __fastcall ModbusReadRegister(unsigned long Address, unsigned short Quantity, unsigned char packet[]);
    unsigned short __fastcall ModbusForceCoil(unsigned long Address, unsigned short Coil);
    unsigned short __fastcall ModbusForceCoils(unsigned long Address, unsigned short Quantity, unsigned char packet[]);
    unsigned short __fastcall ModbusWriteRegister(unsigned long Address, unsigned short Quantity, unsigned char packet[]);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
