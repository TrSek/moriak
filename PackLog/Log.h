//---------------------------------------------------------------------------
// Package pro zapis do log souboru
//---------------------------------------------------------------------------
// Autor:	Zdeno Sekerak
// Modify:	Zdeno Sekerak, Tomas Polacek
// Date:	29.10.2003
//---------------------------------------------------------------------------

#ifndef LogH
#define LogH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <stdio.h>
#include <ScktComp.hpp>
#include <StdCtrls.hpp>
//#include "trayicon.h"

#define SPACE " "
#define MAX_LIST_COUNT 100

#define LOG_FILE "Log"
#define BIN_FILE "Bin"
#define TXT_EXT ".txt"
#define LOG_EXT ".dat"
#define LOG_DIR "LOG"

typedef enum
{
    LOG_ERR,
    LOG_BIN,
    LOG_DEBUG
}ELogType;

//---------------------------------------------------------------------------

class PACKAGE TLog : public TComponent
{
private:
	AnsiString __fastcall GetName(ELogType log_type);
	AnsiString __fastcall GetFileName(ELogType log_type);
	AnsiString __fastcall GetFileNameBak(ELogType log_type, AnsiString poradie);
	AnsiString __fastcall GetSafeFileName(ELogType log_type);
	void __fastcall WriteOldLog(FILE* file, ELogType log_type);
	void __fastcall WriteLineFile(AnsiString line);
	void __fastcall WriteLineObr(AnsiString line);
	void __fastcall InitJenJednou(void);
    static void __fastcall InitSocketMy(void);

	Stdctrls::TMemo *FMemo;
//    TTrayIcon *FTrayIcon;
	bool JenJednou;

	AnsiString log_name;
	AnsiString log_bin;
	AnsiString log_dir;
	AnsiString old_log_dir;
	bool WriteToMemo;

protected:
	bool __fastcall GetWriteToMemo(void);
	void __fastcall SetWriteToMemo(bool aParametr);

public:
	__fastcall TLog(TComponent* Owner);
	__fastcall ~TLog();
	static AnsiString __fastcall GetLocalIP();
	void __fastcall SetLines(TStrings* m_lines);
	void __fastcall SafeSize();
	void __fastcall SaveBin(void* start, int size);
	void __fastcall WriteLog(AnsiString message,int Number = 0);
	void __fastcall Refresh();
	void __fastcall DeleteLog();
	void __fastcall DeleteAllLogs();
	void __fastcall SetLogName(AnsiString aName);
	void __fastcall SetLogBin(AnsiString aBin);
	void __fastcall SetLogDir(AnsiString aDir);
	AnsiString __fastcall GetLogTime();
	AnsiString __fastcall GetLogDate();
	AnsiString __fastcall GetLogIP();

__published:
	__property Stdctrls::TMemo *Memo = { read = FMemo, write = FMemo };
//	__property TTrayIcon *TrayIcon   = { read = FTrayIcon, write = FTrayIcon };
	__property AnsiString Slog_name	 = { read = log_name, write = SetLogName};
	__property AnsiString Slog_bin	 = { read = log_bin,  write = SetLogBin };
	__property AnsiString Slog_dir	 = { read = old_log_dir,  write = SetLogDir };

	__property bool FWriteToMemo	 = { read = GetWriteToMemo,  write = SetWriteToMemo };
};

//---------------------------------------------------------------------------
#endif
