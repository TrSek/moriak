//---------------------------------------------------------------------------
// Package pro zapis do log souboru
//---------------------------------------------------------------------------
// Autor:	Zdeno Sekerak
// Modify:	Zdeno Sekerak, Tomas Polacek
// Date:	29.10.2003
//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "Log.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TLog *)
{
	new TLog(NULL);
}
//---------------------------------------------------------------------------

__fastcall TLog::TLog(TComponent* Owner)
    : TComponent(Owner)
{
	FMemo     = NULL;
//    FTrayIcon = NULL;
	log_name  = LOG_FILE;
	log_bin   = BIN_FILE;
	log_dir   = LOG_DIR;
	old_log_dir = "";

	JenJednou = true;
	WriteToMemo = false;
}
//---------------------------------------------------------------------------

__fastcall TLog::~TLog(void)
{
}
//---------------------------------------------------------------------------

namespace Log
{
	void __fastcall PACKAGE Register()
	{
		TComponentClass classes[1] = {__classid(TLog)};
		RegisterComponents("Elgas", classes, 0);
	}
}
//---------------------------------------------------------------------------


// jaky nazev souboru mu patri
AnsiString __fastcall TLog::GetName(ELogType log_type)
{
	AnsiString name;

	switch( log_type )
	{
		case LOG_ERR:   name = log_name;    break;
		case LOG_BIN:   name = log_bin;     break;
		case LOG_DEBUG: name = log_name;    break;

		default:
			name = log_name;
	}

	// tot vysledek
	return name;
}
//---------------------------------------------------------------------------


// vrati kompletnu cestu a vyrobi spravny nazov suboru
AnsiString __fastcall TLog::GetFileName(ELogType log_type)
{
	return log_dir + "\\" + GetName( log_type ) + GetLogDate() + GetLogIP() + LOG_EXT;
}
//---------------------------------------------------------------------------


// nazov zalozneho souboru
AnsiString __fastcall TLog::GetFileNameBak(ELogType log_type, AnsiString poradie)
{
	if( poradie.Length() == 0 ) poradie = "00";
	if( poradie.Length() == 1 ) poradie = "0" + poradie;

	return log_dir + "\\" + GetName( log_type ) + GetLogDate() + GetLogIP() + "_" + poradie + LOG_EXT;
}
//---------------------------------------------------------------------------


// zisti aktualny datum a cas
AnsiString __fastcall TLog::GetLogTime()
{
	TDateTime cdate;

	cdate = TDateTime::CurrentDateTime();
	return cdate.TimeString();
}
//---------------------------------------------------------------------------


// zisti IP adresu pocitace na kterem bezi
AnsiString __fastcall TLog::GetLocalIP()
{
	HOSTENT *HostEnt;
	AnsiString pom;

	HostEnt = gethostbyname("");

    // neaka chyba, skus opravit
    if( HostEnt == NULL )
    {
        InitSocketMy();
        HostEnt = gethostbyname("");
    }

	pom = Format( "%d.%d.%d.%d", ARRAYOFCONST((
		(byte) HostEnt->h_addr_list[0][0],
		(byte) HostEnt->h_addr_list[0][1],
		(byte) HostEnt->h_addr_list[0][2],
		(byte) HostEnt->h_addr_list[0][3]
		)));

	return pom;
}
//---------------------------------------------------------------------------


// vrati jaky je datum
AnsiString __fastcall TLog::GetLogDate()
{
	TDateTime cdate;

	cdate = TDateTime::CurrentDateTime();
	return cdate.FormatString("yymmdd");
}
//---------------------------------------------------------------------------


// vrati jake je IP pocitace
AnsiString __fastcall TLog::GetLogIP()
{
	HOSTENT *HostEnt;

	HostEnt = gethostbyname("");
    // neni
    if( HostEnt == NULL )
        return "_IP";

	return "_IP" + IntToStr((byte) HostEnt->h_addr_list[0][3]);
}
//---------------------------------------------------------------------------


// aby bola bezpecna velkost lines
// ak ich je vela tak zmaze
void __fastcall TLog::SafeSize()
{
	if( FMemo == NULL )
		return;

	if( FMemo->Lines->Count > MAX_LIST_COUNT )
		FMemo->Lines->Clear();
}
//---------------------------------------------------------------------------


// zapis jednoho riadku do souboru
void __fastcall TLog::WriteLineFile(AnsiString line)
{
	// radek smi obsahovat jenom ASCII
	for( int i=1; i<=line.Length(); i++ )
	{
		if((unsigned char) line[i] < SPACE[0] )
			line[i] = SPACE[0];
	}

	// a ted otevreme soubor
	FILE * file;
	file = fopen( GetFileName( LOG_ERR ).c_str(), "at+" );

	// problem s oteviranim souboru
	if( file == NULL )
    	file = fopen( GetSafeFileName( LOG_ERR ).c_str(), "at+" );

	// ak NULL tak je problem s diskom
	if( file != NULL )
	{
		// prelej stare subory
		WriteOldLog( file, LOG_ERR );

		// zapis
		fprintf( file, "%s\n", line.c_str());
		fclose( file );
	}
}
//---------------------------------------------------------------------------


// zapis jednoho riadku na obrazovku
void __fastcall TLog::WriteLineObr(AnsiString line)
{
    // nejprve to zobrazime
    if(( FMemo != NULL )&&( WriteToMemo ))
    {
        FMemo->Lines->Add( line );
        SafeSize();
    }
}
//---------------------------------------------------------------------------


// zmaze log subor
void __fastcall TLog::DeleteLog()
{
	DeleteFile( GetFileName( LOG_ERR ));
	DeleteFile( GetFileName( LOG_BIN ));
}
//---------------------------------------------------------------------------


// zmaze vsetky log subory
void __fastcall TLog::DeleteAllLogs()
{
TSearchRec sr;

    // vsetky textove logy
    if (FindFirst( log_dir + "\\" + GetName( LOG_ERR ) + "*" + LOG_EXT, (faArchive | faAnyFile), sr) == 0)
    {
        do
        {
            sr.Name = log_dir + "\\" + sr.Name;
            DeleteFile( sr.Name );
        }
        while (FindNext(sr) == 0);
        FindClose(sr);
    }   // if

    // vsetky datove logy
    if (FindFirst( log_dir + "\\" + GetName( LOG_BIN ) + "*" + LOG_EXT, (faArchive | faAnyFile), sr) == 0)
    {
        do
        {
            sr.Name = log_dir + "\\" + sr.Name;
            DeleteFile( sr.Name );
        }
        while (FindNext(sr) == 0);
        FindClose(sr);
    }   // if
}
//---------------------------------------------------------------------------

void __fastcall TLog::SetLogName(AnsiString aName)
{
	log_name = aName;
}
//---------------------------------------------------------------------------

void __fastcall TLog::SetLogBin(AnsiString aBin)
{
	log_bin = aBin;
}
//---------------------------------------------------------------------------

void __fastcall TLog::SetLogDir(AnsiString aDir)
{
	old_log_dir = aDir;

	// absolutni cesta je
//	if( aDir.IsEmpty())
//		log_dir = GetCurrentDir();
//	else
//		log_dir = GetCurrentDir() + "\\" + aDir;

	// absolutni cesta je
	if( aDir.IsEmpty())
        log_dir = ExtractFileDir(ParamStr(0));
	else
		log_dir = ExtractFileDir(ParamStr(0)) + "\\" + aDir;
}
//---------------------------------------------------------------------------

// zapis binarni data do souboru
void __fastcall TLog::SaveBin(void* start, int size)
{
	FILE * file;
	file = fopen( GetFileName( LOG_BIN ).c_str(), "ab+" );

	// problem s oteviranim souboru
	if( file == NULL )
		file = fopen( GetSafeFileName( LOG_BIN ).c_str(), "ab+" );

	// ak NULL tak je problem s diskom
	if( file != NULL )
	{
		// prelej stare subory
		WriteOldLog( file, LOG_BIN );

		// zapis
		fseek( file, 0L, SEEK_END);
		fwrite( start, size, 1, file );
		fclose( file );
    }
}
//---------------------------------------------------------------------------


// zapis chybu do souboru log.dat
void __fastcall TLog::WriteLog(AnsiString message,int Number)
{
	AnsiString line;
	InitJenJednou();
	//
	line = Format( "IP %s, Time %s, %7d,\"%s\"", ARRAYOFCONST((
		GetLocalIP().c_str(),
		GetLogTime().c_str(),
		Number,
		message.c_str()
		)));

	// zapis do souboru
	WriteLineFile( line );

	// jenom chyby
	if( Number >= 0 )
		WriteLineObr( GetLogTime() + " " + message );

    // ma Hint, chce to vidiet
//    if( FTrayIcon != NULL )
//        FTrayIcon->Hint = GetLogTime() + " " + message;
}
//---------------------------------------------------------------------------


// udela refresh obrazovky
void __fastcall TLog::Refresh()
{
	MSG msg;
	while(PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}
//---------------------------------------------------------------------------


void __fastcall TLog::InitJenJednou(void)
{
	if(( FMemo != NULL ) && JenJednou )
	{
		JenJednou = false;
		if( FMemo != NULL )
		{
			FMemo->WordWrap = false;
			FMemo->Enabled = true;
		}
	}
}
//---------------------------------------------------------------------------


// ak povodny log subor sa neda ovorit
// vymysli nazov ktory neexistuje a zapise to don
AnsiString __fastcall TLog::GetSafeFileName(ELogType log_type)
{
	AnsiString name = "";
	AnsiString last_name;
	FILE* file;
	int i=0;

	// najde posledny a dalsi novy
	do
	{
		name = GetFileNameBak( log_type, IntToStr( ++i ));
	}
	while(( i<MAX_LIST_COUNT ) && FileExists( name ));

	// rozhodne ci je mozne pouzit posledny
	last_name = GetFileNameBak( log_type, IntToStr( i-1 ));
	file = fopen( last_name.c_str(), "r" );

	// ide to
	if( file != NULL )
	{
		fclose( file );
		name = last_name;
	}

	// tot vytuzeny nazov suboru
	return name;
}
//---------------------------------------------------------------------------


// najde vsechni zalozne soubory a zapise do puvodneho
void __fastcall TLog::WriteOldLog(FILE* file, ELogType log_type)
{
	FILE* fileOld;
	TSearchRec sr;
	AnsiString name;
	char ch;

	// takuto hrozu hladam
   	name = GetFileNameBak( log_type, "*" );

	// je neaky
	if( FindFirst( name, faAnyFile, sr ) != 0 )
		return;

	do {
		sr.Name = log_dir + "\\" + sr.Name;
		fileOld = fopen( sr.Name.c_str(), "r" );
		fseek( fileOld, 0, SEEK_SET );

		// prelej do povodneho
		if( fileOld != NULL )
		{
			while( !feof( fileOld ))
			{
				ch = fgetc( fileOld );
				if( !feof( fileOld ))
					fputc( ch, file );
			}
		}

		// zavri - zmaz
		fclose( fileOld );
		DeleteFile( sr.Name );
	}
	while( FindNext(sr) == 0 );
}
//---------------------------------------------------------------------------

// Vrat nastaveni zapisovat do Memo boxu
bool __fastcall TLog::GetWriteToMemo(void)
{
	return WriteToMemo;
}
//---------------------------------------------------------------------------

// Nastaveni zapisovat do Memo boxu
void __fastcall TLog::SetWriteToMemo(bool aParametr)
{
	WriteToMemo = aParametr;
}
//---------------------------------------------------------------------------

// neaka chyba suvisiaca s TCP/IP
void __fastcall TLog::InitSocketMy(void)
{
    // soket nieje inicializovany?
    if( WSAGetLastError() == WSANOTINITIALISED )
    {
    WORD wVersionRequested;
    WSADATA wsaData;

        wVersionRequested = MAKEWORD( 2, 0 );
        WSAStartup( wVersionRequested, &wsaData );
    }
}
//---------------------------------------------------------------------------

